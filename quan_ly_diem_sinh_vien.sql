-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 14, 2021 lúc 09:56 PM
-- Phiên bản máy phục vụ: 10.4.19-MariaDB
-- Phiên bản PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quan_ly_diem_sinh_vien`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `grades`
--

CREATE TABLE `grades` (
  `grade_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL,
  `grade` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `grades`
--

INSERT INTO `grades` (`grade_id`, `student_id`, `subject_id`, `teacher_id`, `term_id`, `grade`) VALUES
(1, 1, 1, 1, 1, 'A'),
(2, 1, 2, 2, 1, 'B'),
(3, 1, 3, 3, 2, 'C'),
(4, 1, 4, 4, 2, 'F'),
(5, 1, 5, 5, 3, 'A+'),
(6, 1, 6, 6, 3, 'C+'),
(7, 2, 1, 1, 1, 'C'),
(8, 2, 2, 2, 1, 'D'),
(9, 2, 3, 3, 2, 'A'),
(10, 2, 4, 4, 2, 'F'),
(11, 2, 5, 5, 3, 'A+'),
(12, 2, 6, 6, 3, 'C'),
(13, 3, 1, 1, 1, 'B'),
(14, 3, 2, 2, 1, 'C'),
(15, 3, 3, 3, 2, 'D+'),
(16, 3, 4, 4, 2, 'A'),
(17, 3, 5, 5, 3, 'B+'),
(18, 3, 6, 6, 3, 'A'),
(19, 4, 1, 1, 1, 'C'),
(20, 4, 2, 2, 1, 'A+'),
(21, 4, 3, 3, 2, 'B'),
(22, 4, 4, 4, 2, 'A'),
(23, 4, 5, 5, 3, 'D'),
(24, 4, 6, 6, 3, 'C+'),
(25, 5, 1, 1, 1, 'F'),
(26, 5, 2, 2, 1, 'D+'),
(27, 5, 3, 3, 2, 'F'),
(28, 5, 4, 4, 2, 'D'),
(29, 5, 5, 5, 3, 'A'),
(30, 5, 6, 6, 3, 'B'),
(31, 6, 1, 1, 1, 'C+'),
(32, 6, 2, 2, 1, 'D'),
(33, 6, 3, 3, 2, 'C'),
(34, 6, 4, 4, 2, 'D'),
(35, 6, 5, 5, 3, 'D+'),
(36, 6, 6, 6, 3, 'C');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `student_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `students`
--

INSERT INTO `students` (`id`, `student_name`) VALUES
(1, 'Khuất Văn An'),
(2, 'Lê Văn Lâm'),
(3, 'Kiều Huy Vũ'),
(4, 'Khuất Thị Hoa'),
(5, 'Vũ Nguyên Đạt'),
(6, 'Nguyễn Văn Trường'),
(7, 'Đặng Huy Anh'),
(8, 'Kiều Xuân Bắc'),
(9, 'Nguyễn Tự Long'),
(10, 'Nguyễn Trấn Thành'),
(11, 'Vũ Công Lý'),
(12, 'Khuất Thị Vân Dung'),
(13, 'Kiều Trường Giang'),
(14, 'Khuất Phương Thanh'),
(15, 'Tô Văn Vũ'),
(16, 'Nguyễn Văn A'),
(17, 'Nguyễn Văn C');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `subjects`
--

INSERT INTO `subjects` (`id`, `teacher_id`, `subject_name`) VALUES
(1, 1, 'Thông tin số 1 '),
(2, 2, 'Kiến trúc máy tính'),
(3, 3, 'Xử lý số tín hiệu'),
(4, 4, 'Thiết kế số VHDL'),
(5, 5, 'Mạch tuyến tính 2'),
(6, 6, 'Cơ sở mạng dữ liệu');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `teacher_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `teachers`
--

INSERT INTO `teachers` (`id`, `teacher_name`) VALUES
(1, 'Ngô Văn Đức '),
(2, 'Tạ Thị Kim Huệ'),
(3, 'Phạm Văn Tiến'),
(4, 'Vũ Văn Yêm'),
(5, 'Cung Thành Long'),
(6, 'Nguyễn Việt Anh');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `term` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `terms`
--

INSERT INTO `terms` (`id`, `term`) VALUES
(1, 'Kì 1'),
(2, 'Kì 2'),
(3, 'Kì 3');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`grade_id`),
  ADD KEY `fk_student` (`student_id`),
  ADD KEY `fk_subject_grade` (`subject_id`),
  ADD KEY `fk_teacher` (`teacher_id`),
  ADD KEY `fk_term` (`term_id`);

--
-- Chỉ mục cho bảng `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subject` (`teacher_id`);

--
-- Chỉ mục cho bảng `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `grades`
--
ALTER TABLE `grades`
  MODIFY `grade_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT cho bảng `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `fk_student` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `fk_subject_grade` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`),
  ADD CONSTRAINT `fk_teacher` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`),
  ADD CONSTRAINT `fk_term` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`);

--
-- Các ràng buộc cho bảng `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `fk_subject` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
