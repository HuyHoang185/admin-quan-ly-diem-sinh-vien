<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="libs/css/admin.css" rel="stylesheet">
    <link href="libs/css/bootstrap.min.css" rel="stylesheet">
    <script src="libs/js/jquery-3.5.1.min.js"></script>
    <script src="libs/js/bootstrap.min.js"></script>
    <script src="libs/ckeditor/ckeditor.js"></script>
</head>
<body>
    <!-- header -->
    <div id="header" >
       <div class="head">
          <div>
             <a href="index.php"><span>Admin </span></a>
          </div>         
       </div>
    </div>

    <!-- menu -->
    <div id="menu">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-3 list ">
                    <a href="index.php?page_layout=grade" class="">Grade</a>
                </div>
                <div class="col-lg-3 list">
                    <a href="index.php?page_layout=student" class="">Student</a>
                </div>
                <div class="col-lg-3 list">
                    <a href="index.php?page_layout=teacher">Teacher</a>
                </div>
                <div class="col-lg-3 list">
                    <a href="index.php?page_layout=subject">Subject</a>
                </div>          
            </div>
        </div>
    </div>
    <?php
       include_once("controllers/admin.php");
       
    ?>
</body>
</html>