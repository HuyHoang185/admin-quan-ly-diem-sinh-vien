   <!-- body -->
    <div id="add_product">
        <div class="container">
            <div class=" row place">
                <p>Home | Product | Thêm sản phẩm</p>
            </div>
            <div style="margin:40px 0 40px -15px;"><h3>Thêm sản phẩm</h3></div>
            <div class="row">
                <div class="form-add">
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Tên học sinh</label>
                            <input name="student_name" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label>Tên môn học</label>
                            <input name="subject_name" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label>Tên giáo viên</label>
                            <input name="teacher_name" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label>kì học</label>
                            <input name="term" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label>Điểm</label>
                            <input name="grade" type="text" value="">
                        </div>                  
                        <div class="form-group">
                            <input name="sbm" type="submit" value="Thêm mới">
                        </div>
                        <?php
                          if(isset($_POST['sbm'])){
                            $student_name=$_POST['student_name'];
                            $subject_name=$_POST['subject_name'];
                            $teacher_name=$_POST['teacher_name'];
                            $term=$_POST['term'];
                            $grade=$_POST['grade'];
                            switch($student_name){
                                case "Khuất Văn An":$student_id=1;break;
                                case "Lê Văn Lâm":$student_id=2;break;
                                case "Kiều Huy Vũ":$student_id=3;break;
                                case "Khuất Thị Hoa":$student_id=4;break;
                                case "Vũ Nguyên Đạt":$student_id=5;break;
                                case "Nguyễn Văn Trường":$student_id=6;break;
                                default :$student_id=null;
                            }
                            switch($subject_name){
                                case "Thông tin số 1":$subject_id=1;break;
                                case "Kiến trúc máy tính":$subject_id=2;break;
                                case "Xử lý số tín hiệu":$subject_id=3;break;
                                case "Thiết kế số VHDL":$subject_id=4;break;
                                case "Mạch tuyến tính 2":$subject_id=5;break;
                                case "Cơ sở mạng dữ liệu":$subject_id=6;break;
                                default :$subject_id=null;
                            }
                            switch($teacher_name){
                                case "Ngô Văn Đức":$teacher_id=1;break;
                                case "Tạ Thị Kim Huệ":$teacher_id=2;break;
                                case "Phạm Văn Tiến":$teacher_id=3;break;
                                case "Vũ Văn Yêm":$teacher_id=4;break;
                                case "Cung Thanh Long":$teacher_id=5;break;
                                case "Nguyễn Việt Anh":$teacher_id=6;break;
                                default :$teacher_id=null;
                            }
                            switch($term){
                                case "Kì 1":$term_id=1;break;
                                case "Kì 2":$term_id=2;break;
                                case "Kì 3":$term_id=3;break;
                                default :$term_id=null;
                            }
                            include_once('./models/m_add_grade.php');
                          } 
                        ?>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
