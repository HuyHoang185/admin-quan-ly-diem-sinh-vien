<?php
include_once("./models/db.php");
if(isset($_GET['page_layout'])){
    switch ($_GET['page_layout']){
       case "add_student":include_once("./views/add_student.php");break;
       case "edit_student":include_once("./views/edit_student.php");break;
       case "edit_grade":include_once("./views/edit_grade.php");break;
       case "edit_teacher":include_once("./views/edit_teacher.php");break;
       case "edit_subject":include_once("./views/edit_subject.php");break;
       case "add_grade":include_once("./views/add_grade.php");break;
       case "add_teacher":include_once("./views/add_teacher.php");break;
       case "add_subject":include_once("./views/add_subject.php");break;
       case "grade":include_once("./views/grade.php");break;
       case "student":include_once("./views/student.php");break;
       case "teacher":include_once("./views/teacher.php");break;
       case "subject":include_once("./views/subject.php");break;
       case "del_grade":include_once("./models/del_grade.php");break;
       case "del_student":include_once("./models/del_student.php");break;
       case "del_teacher":include_once("./models/del_teacher.php");break;
       case "del_subject":include_once("./models/del_subject.php");break;
    }
}else {
    include_once("./views/grade.php");
}

?>