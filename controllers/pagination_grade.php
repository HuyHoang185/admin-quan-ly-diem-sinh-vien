<?php
include_once("./models/db.php");
if(isset($_GET["page"])){
    $page=$_GET["page"];
}else{
    $page=1;
}
$row_per_page=5;
$per_row=$page * $row_per_page-$row_per_page ;
$total_row= mysqli_num_rows(mysqli_query($conn,"SELECT * FROM grade"));
$total_page= ceil($total_row/$row_per_page);
$list_page ='';
$page_prev= $page-1;
if($page_prev<=0){
    $page_prev=1;
}
$list_page.= '<li class="page-item"><a class="page-link" href="admin-diem-sinh-vien/index.php?page_layout=grade&page='.$page_prev.'">&laquo;</a></li>' ;
for($i=1;$i<=$total_page;$i++){
    if($page==$i){
        $active='active';
    }
    else{
        $active='';
    }
    $list_page.= '<li class="page-item '.$active.'"><a  class="page-link" href="admin-diem-sinh-vien/index.php?page_layout=grade&page='.$i.'">'.$i.'</a></li>';
}
$page_next =$page+1;
if($page_next>$total_page){
    $page_next=$total_page;
}
$list_page.= '<li class="page-item"><a class="page-link" href="admin-diem-sinh-vien/index.php?page_layout=grade&page='.$page_next.'">&raquo;</a></li>' ;
?>